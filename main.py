
#!/usr/bin/python3.5

import sys

def main():

    fn = sys.argv[1]
    target_sum = sys.argv[2]
    
    (l_raw,l_value) = input_process(fn)
    
    (idx1,idx2) = find_pair(l_value,target_sum)
    
    if ( (idx1 == -1) and (idx2 == -1)):
        print ("Not possible")        
    else:
        print (l_raw[idx1]+", "+l_raw[idx2]+"\n")


def input_process(fn):
    with open(fn) as f:
        lines = f.read().splitlines()
    
    value_list = []
    for val in lines:
        (key, val) = val.split()
        value_list.append(val)
    return (lines,value_list)

def find_pair(input_list,target_sum):
    
    (total1,total2, total3) = (0,0,0)
    length = len(input_list)
    i = 0
    j = length - 1
    temp_sum = input_list[i] + input_list[j]
    (x,y) = (-1,-1)
    
    while (i < j):
        total1 = input_list[i] + input_list[j]
        if ( (i+1) != j ):
            total2 = input_list[i+1] + input_list[j]
            total3 = input_list[i] + input_list[j-1]
        
        if (total1 >= target_sum) and (total1 < temp_sum):
            if (total1 == target_sum):
                (x,y) = (i,j)
            else:
                temp_sum = total1
                (x,y) = (i,j)
        if (total2 >= target_sum) and (total2 < temp_sum):
            if (total2 == target_sum):
                (x,y) = (i,j)
            else:
                temp_sum = total2
                (x,y) = (i,j)
        if (total3 >= target_sum) and (total3 < temp_sum):
            if (total3 == target_sum):
                (x,y) = (i,j)
            else:
                temp_sum = total3
                (x,y) = (i,j)
        i = i + 1
        j = j - 1


    return (x,y)

if __name__ == "__main__":
    # execute only if run as a script
    main()